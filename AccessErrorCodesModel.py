from pymongo import MongoClient
from flask import Flask, jsonify, request 
from flask_cors import CORS
import json
import jsons
from bson import json_util
from bson.json_util import dumps

def ManufacturerErrorTest(event, context):
    Manufacturer = event['Manufacturer']
    Model = event['Model']
    client = MongoClient("mongodb+srv://test:test@cluster0.gzucq.mongodb.net/test?retryWrites=true&w=majority")
    db = client.get_database('ErrorCodes')
    records = db.VendorErrorCodes
    if Manufacturer != "" and Model != "":
        records1 = dumps(records.find({"Manufacturer": Manufacturer,"Model": Model}))
    elif Manufacturer != "" and Model == "":
        records1 = dumps(records.find({"Manufacturer": Manufacturer}))
    else:
        records1 = dumps(records.find({ }))
    
    json_data = json.loads(records1)
    
    return {
        'statusCode': 200,
        'headers': {
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,GET'
        },
        'body': json_data
    }
